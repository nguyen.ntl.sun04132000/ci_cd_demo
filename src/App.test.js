import { render, screen } from '@testing-library/react';
import App, {add, is_ghe_Quyen} from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test("add function", ()=>{
  expect(add(1,2)).toBe(3);
  expect(add(2,3)).toBe(5);
})